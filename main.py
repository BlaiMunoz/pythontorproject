# *-* cp1252 *-*        
from collections import OrderedDict
import pandas as pd
from datetime import datetime as dt

def getNumberOfNodes():
    numeroNodos = 0
    with open('/var/lib/tor/cached-microdesc-consensus') as archivo:
        for linea in archivo:
            linea = linea.rstrip()
            if linea.startswith("r "):
                lineaR = linea.split(' ')
                direccionIp = lineaR[5]

            if linea.startswith("w "):
                numeroNodos = numeroNodos + 1
                lineaW = linea.replace('w Bandwidth=', '')
                velocidad = lineaW.split(' ')
                diccionario[direccionIp] = int(velocidad[0])
    return numeroNodos

def getMaxSpeed():
    n = 0
    for ip in enumerate(nodosPorVelocidad):
        if n == 0:
            maxIpSpeed = diccionario[ip[1]]
            maxIp = ip[1]
        if n == 100:
            break
        print("{:20}".format(ip[1]), ':  ', diccionario[ip[1]])
        n = n + 1
    return maxIpSpeed, maxIp


if __name__ == '__main__':
    diccionario = {}

    numeroNodos = getNumberOfNodes()

    nodosPorVelocidad = OrderedDict(sorted(diccionario.items(), key=lambda x:x[1], reverse= True))

    maxIpSpeed, maxIp = getMaxSpeed()

    data = [[dt.now(), numeroNodos, maxIp, maxIpSpeed]]
    df = pd.DataFrame(data, columns=["time", "numero de nodos", "ip maxima velocidad", "velocidad"])
    print(df)
    df.to_csv("nodosTor.csv", mode= 'a', index= False, header= False)
